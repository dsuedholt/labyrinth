package client;

import game.Card;
import generated.LoginMessageType;
import generated.MazeCom;
import generated.MazeComType;
import generated.MoveMessageType;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.Socket;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import networking.UTFInputStream;
import networking.UTFOutputStream;
import ai.AI;
import ai.ActualAI;

public class TCPClient {

	private Socket clientSocket;
	private UTFOutputStream outToServer;
	private UTFInputStream inFromServer;
	private Marshaller marshaller;
	private Unmarshaller unmarshaller;
	private AI player;
	private boolean gameover;
	private int id;
	private boolean login;


	public void connect(String host, int port) throws IOException{
		createMarshallers();

		gameover = false;

		player = new ActualAI();

		clientSocket = new Socket(host, port);

		outToServer = new
				UTFOutputStream(clientSocket.getOutputStream());

		inFromServer = new UTFInputStream(clientSocket.getInputStream());

		login = false;

	}

	public void disconnect() throws IOException{
		clientSocket.close();
	}

	public void send(String sentence) throws IOException{
		outToServer.writeUTF8(sentence);
	}

	public void receiveMessage(){
		String serverAnswer;
		MazeCom mc;
		try {
			serverAnswer = inFromServer.readUTF8();

			ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(
					serverAnswer.getBytes());
			mc = (MazeCom) unmarshaller.unmarshal(byteArrayInputStream);

			handleMessage(mc);	

		} catch (IOException e) {
			System.out.println("Connection lost!");
			try {
				disconnect();
			} catch (IOException e1) {
				System.out.println("Unable to disconnect!");
				e1.printStackTrace();
			}
			exitClient();
		} catch (JAXBException e2){
			e2.printStackTrace();
		}
	}

	public void exitClient(){
		System.exit(0);
	}

	public void startClient(){
		String serverAnswer;
		MazeCom mc;
		
		attemptLogin();
		while (!gameover){
			try {
				serverAnswer = inFromServer.readUTF8();

				ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(
						serverAnswer.getBytes());
				mc = (MazeCom) unmarshaller.unmarshal(byteArrayInputStream);

				handleMessage(mc);	

				if (!login) break;

			} catch (IOException e) {
				System.out.println("Unable to read!");
			} catch (JAXBException e2){
				e2.printStackTrace();
			}

		}

		System.out.println("The game is over!");
		System.exit(0);
	}

	private void attemptLogin() {

		MazeCom mc = new MazeCom();
		LoginMessageType lm = new LoginMessageType();
		lm.setName("Serenity");

		mc.setMcType(MazeComType.LOGIN);
		mc.setLoginMessage(lm);

		send(mc);
	}

	public void createMarshallers(){
		JAXBContext jc;
		try {
			jc = JAXBContext.newInstance(MazeCom.class);
			marshaller = jc.createMarshaller();
			unmarshaller = jc.createUnmarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}

	public void handleMessage(MazeCom mc){
		switch(mc.getMcType()){
		case LOGINREPLY: 
			id = mc.getLoginReplyMessage().getNewID();
			player.setId(id);
			login = true;
			System.out.println("Login erfolgreich");
			break;
		case AWAITMOVE: 
			player.setBoard(mc.getAwaitMoveMessage().getBoard());
			player.setTreasure(mc.getAwaitMoveMessage().getTreasure());
			player.setTreasuresToGo(mc.getAwaitMoveMessage().getTreasuresToGo());
			sendNextMove();
			break;
		case WIN: 
			int winner = mc.getWinMessage().getWinner().getId();
			if(winner==id){
				System.out.println("Du hast gewonnen!");
			} else {
				System.out.println("Der Gewinner ist Spieler "+winner);
			}
			gameover = true;
			break;
		case ACCEPT: 
			if (!mc.getAcceptMessage().isAccept()){
				System.out.println(mc.getAcceptMessage().getErrorCode().value());
			}
			break;
		default: break;
		}

	}

	public void sendNextMove(){
		MoveMessageType mt = player.getNextMove();

		System.out.printf("Shift at %d, %d\n", mt.getShiftPosition().getRow(), mt.getShiftPosition().getCol());
		System.out.println("Card");
		System.out.println(new Card(mt.getShiftCard()));
		System.out.printf("Move to %d, %d\n", mt.getNewPinPos().getRow(), mt.getNewPinPos().getCol());

		MazeCom mc = new MazeCom();
		mc.setMcType(MazeComType.MOVE);

		mc.setMoveMessage(mt);

		send(mc);	
	}

	private void send(MazeCom mc) {
		mc.setId(id);
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		try {
			marshaller.marshal(mc, byteArrayOutputStream);
		} catch (JAXBException e1) {
			e1.printStackTrace();
		}
		String clientString = new String(byteArrayOutputStream.toByteArray());

		try {
			outToServer.writeUTF8(clientString);
		} catch (IOException e) {
			System.out.println("Unable to send!");
			e.printStackTrace();
		}	
	}

	public static void main(String args[]){
		TCPClient client = new TCPClient();
		if (args.length != 2) {
			System.out.println("Usage: java -jar Serenity.jar [serveraddress] [serverport]");
			System.exit(0);
		}
		try {
			client.connect(args[0], Integer.parseInt(args[1]));
			client.startClient();
		} catch (IOException e) {
			System.out.println("Unable to connect Client to Server!");
			e.printStackTrace();
		}

	}
}
