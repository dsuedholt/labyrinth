package ai;

public class Values {
	public static double TREASURES_AVAILABLE = 10;
	public static double TREASURE_REACHED = 1000;
	public static double NO_TREASURE = 1000;
	public static double WALLS_TO_TREASURE = 2;
	public static double DISTANCE_TO_TREASURE = 1;
	public static double IMMOVABLE = 2.5;
	public static double ENEMY_CAN_FINISH = 10000;
	public static double WINNER = 10000000;
}
