package ai;

import game.Card;
import generated.CardType;
import generated.MoveMessageType;
import generated.PositionType;

import java.util.List;
import java.util.Scanner;

public class HumanAI extends AI {
	@Override
	public MoveMessageType getNextMove() {
		System.out.println("ID: " + id);
		CardType ct = boardType.getShiftCard();
		Card card = new Card(ct);
		List<Card> rots = card.getPossibleRotations();
		for (int i = 0; i < rots.size(); i++) {
			System.out.println(i);
			System.out.println(rots.get(i));
		}
		Scanner sc = new Scanner(System.in);
		int rot = Integer.parseInt(sc.nextLine());
		card = rots.get(rot);
		System.out.println("Einschieben bei: ");
		String[] ans = sc.nextLine().split(" ");
		int r = Integer.parseInt(ans[0]);
		int c = Integer.parseInt(ans[1]);
		PositionType shift = of.createPositionType();
		shift.setRow(r);
		shift.setCol(c);
		PositionType pin = of.createPositionType();
		MoveMessageType res = of.createMoveMessageType();
		System.out.println("Ziehen nach:");
		ans = sc.nextLine().split(" ");
		r = Integer.parseInt(ans[0]);
		c = Integer.parseInt(ans[1]);
		pin.setRow(r);
		pin.setCol(c);
		res.setNewPinPos(pin);
		ct.setOpenings(card.getOpenings());
		res.setShiftCard(ct);
		res.setShiftPosition(shift);
		return res;
	}
}
