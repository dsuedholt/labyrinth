package ai;

import game.Board;
import generated.BoardType;
import generated.CardType;
import generated.PositionType;
import generated.TreasureType;
import generated.TreasuresToGoType;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TreasureKeeper {
	private Set<TreasureType> availableTreasures;
	private int[] playerTreasures;

	public TreasureKeeper(List<TreasuresToGoType> trToGo) {
		availableTreasures = new HashSet<TreasureType>();
		for (TreasureType t : TreasureType.values()) {
			availableTreasures.add(t);
		}

		availableTreasures.remove(TreasureType.START_01);
		availableTreasures.remove(TreasureType.START_02);
		availableTreasures.remove(TreasureType.START_03);
		availableTreasures.remove(TreasureType.START_04);

		try {
			playerTreasures = new int[trToGo.size()];
			for (TreasuresToGoType tr : trToGo) {
				playerTreasures[tr.getPlayer() - 1] = tr.getTreasures();
			}
		}
		catch (ArrayIndexOutOfBoundsException e) {
			// spieler hat das spiel verlassen
		}
	}

	public Set<TreasureType> getAvailableTreasures() {
		return availableTreasures;
	}

	public void update(List<TreasuresToGoType> trToGo, BoardType bt) {
		Board b = new Board(bt);
		try {
			for (TreasuresToGoType t : trToGo) {
				if (t.getTreasures() != playerTreasures[t.getPlayer() - 1]) {
					playerTreasures[t.getPlayer() - 1] = t.getTreasures();
					PositionType p = b.findPlayer(t.getPlayer());
					CardType c = b.getCard(p.getRow(), p.getCol());
					availableTreasures.remove(c.getTreasure());
				}
			}
		}
		catch (ArrayIndexOutOfBoundsException e) {
			// ein spieler hat das spiel verlassen
		}
	}
}
