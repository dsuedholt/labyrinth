package ai;

import game.Board;
import generated.BoardType;
import generated.MoveMessageType;
import generated.ObjectFactory;
import generated.PositionType;
import generated.TreasureType;
import generated.TreasuresToGoType;

import java.util.List;
import java.util.Random;

public class AI {
	
	protected BoardType boardType;
	protected Board board;
	protected TreasureType treasure;
	protected List<TreasuresToGoType> treasuresToGo;
	protected int id;
	
	protected ObjectFactory of = new ObjectFactory();
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public BoardType getBoard() {
		return boardType;
	}

	public void setBoard(BoardType board) {
		this.boardType = board;
	}

	public TreasureType getTreasure() {
		return treasure;
	}

	public void setTreasure(TreasureType treasure) {
		this.treasure = treasure;
	}

	public List<TreasuresToGoType> getTreasuresToGo() {
		return treasuresToGo;
	}

	public void setTreasuresToGo(List<TreasuresToGoType> treasuresToGo) {
		this.treasuresToGo = treasuresToGo;
	}

	public MoveMessageType getNextMove() {
		board = new Board(boardType);
		PositionType myPos = board.findPlayer(id);
		int ichR = myPos.getRow(), ichC = myPos.getCol();
		PositionType forbidden = board.getForbidden();
		int noR = -1, noC = -1;
		if (forbidden != null) {
			noR = forbidden.getRow();
			noC = forbidden.getCol();
		}
		PositionType shift = new PositionType();
		int r = 0, c = 0;
		Random rnd = new Random();
		
		while (r % 2 == 0 || r == noR) {
			r = rnd.nextInt(7);
		}
		shift.setCol(c);
		shift.setRow(r);
		MoveMessageType res = of.createMoveMessageType();
		res.setNewPinPos(myPos);
		res.setShiftCard(boardType.getShiftCard());
		res.setShiftPosition(shift);
		return res;
	}
}
