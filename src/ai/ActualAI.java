package ai;

import game.Board;
import game.Card;
import game.Position;
import generated.CardType;
import generated.CardType.Openings;
import generated.MoveMessageType;
import generated.PositionType;
import generated.TreasureType;
import generated.TreasuresToGoType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;

public class ActualAI extends AI {

	private PositionType shift, pinPos;
	private List<PositionType> allShiftPositions;
	private Card card;
	private TreasureKeeper trKpr;
	private int players;
	private final int DEPTH = 2;
	private final int TIMEOUT = 19200;
	private Map<PositionType, List<Node>> neighbors;
	Node[][] nodes;

	@Override
	public void setTreasuresToGo(List<TreasuresToGoType> treasuresToGo) {
		if (trKpr == null) trKpr = new TreasureKeeper(treasuresToGo);
		else trKpr.update(treasuresToGo, boardType);
		this.treasuresToGo = treasuresToGo;
	}

	public ActualAI() {
		allShiftPositions = new ArrayList<PositionType>();
		for (int i = 1; i < 6; i += 2) {
			PositionType tmp = of.createPositionType();
			tmp.setRow(i);
			tmp.setCol(0);
			allShiftPositions.add(tmp);
			tmp = of.createPositionType();
			tmp.setRow(0);
			tmp.setCol(i);
			allShiftPositions.add(tmp);
			tmp = of.createPositionType();
			tmp.setRow(6);
			tmp.setCol(i);
			allShiftPositions.add(tmp);
			tmp = of.createPositionType();
			tmp.setRow(i);
			tmp.setCol(6);
			allShiftPositions.add(tmp);
		}
		neighbors = new HashMap<PositionType, List<Node>>();
		nodes = new Node[7][7];
		for (int i = 0; i < 7; i++) {
			for (int j = 0; j < 7; j++)
				nodes[i][j] = new Node();
		}
		for (int i = 0; i < 7; i++) {
			for (int j = 0; j < 7; j++) {
				PositionType tmp = of.createPositionType();
				tmp.setRow(i);
				tmp.setCol(j);
				nodes[i][j].pos = tmp;
				List<Node> l = new ArrayList<Node>();
				for (int r = -1; r <= 1; r++) {
					for (int c = -1; c <= 1; c++) {
						if (Math.abs(r) + Math.abs(c) != 1) continue;
						if (!validPos(i + r, j + c)) continue;
						l.add(nodes[i + r][j + c]);
					}
				}
				neighbors.put(tmp, l);
			}
		}
	}

	private class CalcThread extends Thread {
		Board bestMove;
		ActualAI callback;

		public CalcThread(ActualAI callback) {
			this.callback = callback;
		}

		@Override
		public void run() {
			max(board, id, DEPTH, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
			synchronized (callback) {
				callback.notify();
			}
		}

		private double max(Board board, int player, int depth, double alpha, double beta) {
			if (isInterrupted()) {
				if (bestMove == null) bestMove = board;
				return 0;
			}
			if (depth == 0 || onTreasure(board, player)) return evaluate(board);
			double bestVal = alpha;
			for (Board b : generateMoves(board, player)) {
				int nextPlayer = (player % players) + 1;
				double val;
				if (nextPlayer == id)
					val = max(b, nextPlayer, depth - 1, bestVal, beta);
				else
					val = min(b, nextPlayer, depth - 1, bestVal, beta);
				if (val > bestVal) {
					bestVal = val;
					if (bestVal >= beta)
						break;
					if (depth == DEPTH) {
						bestMove = b;
					}
				}
			}
			return bestVal;
		}

		private double min(Board board, int player, int depth, double alpha, double beta) {
			if (isInterrupted()) {
				if (bestMove == null) bestMove = board;
				return 0;
			}
			if (depth == 0) return evaluate(board);
			double worstVal = beta;
			for (Board b : generateMoves(board, player)) {
				int nextPlayer = (player % players) + 1;
				double val;
				if (nextPlayer == id)
					val = max(b, nextPlayer, depth - 1, alpha, worstVal);
				else
					val = min(b, nextPlayer, depth - 1, alpha, worstVal);
				if (val < worstVal) {
					worstVal = val;
					if (worstVal <= alpha) break;
				}
			}
			return worstVal;
		}
	}

	@Override
	public MoveMessageType getNextMove() {
		Collections.shuffle(allShiftPositions);
		players = treasuresToGo.size();
		board = new Board(boardType);
		MoveMessageType res = of.createMoveMessageType();
		card = new Card(boardType.getShiftCard());
		shift = of.createPositionType();
		pinPos = of.createPositionType();
		findOptimalMove();
		CardType ct = of.createCardType();
		ct.setOpenings(card.getOpenings());
		ct.setPin(card.getPin());
		ct.setTreasure(card.getTreasure());
		res.setShiftCard(ct);
		res.setNewPinPos(pinPos);
		res.setShiftPosition(shift);
		return res;
	}

	private synchronized void findOptimalMove() {
		CalcThread calc = new CalcThread(this);
		calc.start();
		try {
			wait(TIMEOUT);
			calc.interrupt();
		} catch (InterruptedException e) {
			System.out.println("Something, somewhere, went horribly wrong.");
		}
		saveMove(calc.bestMove);
	}

	private void saveMove(Board b) {
		shift = getShiftPosition(b);
		card = new Card(b.getCard(shift.getRow(), shift.getCol()));
		card.getPin().getPlayerID().clear();
		pinPos = b.findPlayer(id);
	}

	private PositionType getShiftPosition(Board b) {
		Position f = (Position) b.getForbidden();
		return f.getOpposite();
	}

	private List<Board> generateMoves(Board b, int player) {
		LinkedList<Board> res = new LinkedList<Board>();
		MoveMessageType move = of.createMoveMessageType();
		CardType ct = b.getShiftCard();
		move.setShiftCard(ct);
		List<Card> rotatoes = new Card(ct).getPossibleRotations();
		for (PositionType tmpShift : allShiftPositions) {
			if (b.getForbidden() != null && b.getForbidden().equals(tmpShift)) continue;
			move.setShiftPosition(tmpShift);
			for (Card c : rotatoes) {
				ct.setOpenings(c.getOpenings());
				Board tmpBoard = b.fakeShift(move);
				PositionType myPos = tmpBoard.findPlayer(player);
				PositionType trPos = tmpBoard.findTreasure(treasure);
				for (PositionType pt : tmpBoard.getAllReachablePositions(myPos)) {
					tmpBoard.movePlayer(myPos, pt, player);
					if (id == player && trPos != null && myPos.equals(trPos))
						res.addFirst((Board)tmpBoard.clone());
					else
						res.add((Board)tmpBoard.clone());
					tmpBoard.movePlayer(pt, myPos, player);
				}
			}
		}
		return res;
	}


	private double evaluate(Board b) {
		double val = 0;
		PositionType myPos = b.findPlayer(id);
		PositionType trPos = b.findTreasure(treasure);
		if (myPos.getRow() % 2 == 0 && myPos.getCol() % 2 == 0) {
			val += Values.IMMOVABLE;
		}
		int allTreasures = trKpr.getAvailableTreasures().size();
		if (trPos == null) // haben unseren schatz herausgeschoben
			val -= Values.NO_TREASURE;
		else {
			int dr = Math.abs(myPos.getRow() - trPos.getRow());
			int dc = Math.abs(myPos.getCol() - trPos.getCol());
			val -= Values.DISTANCE_TO_TREASURE * (dr + dc);
			if (myPos.equals(trPos)) {
				if (treasure == TreasureType.START_01 ||
					treasure == TreasureType.START_02 ||
					treasure == TreasureType.START_03 ||
					treasure == TreasureType.START_03)
					val += Values.WINNER;
				else {
					val += Values.TREASURE_REACHED;
					val += (getReachableTreasures(myPos, b) * 1. / allTreasures) * Values.TREASURES_AVAILABLE;
				}
			}
			else 
				val -= Values.WALLS_TO_TREASURE * getWallsToTreasure(myPos, trPos, b);
		}
		//guete der position des naechsten gegners
		int enemy = (id % players) + 1;
		myPos = b.findPlayer(enemy);

		int treasures = treasuresToGo.get(enemy - 1).getTreasures();
		if (treasures == 1) {
			TreasureType treasure = null;
			switch (enemy) {
			case 1: treasure = TreasureType.START_01;
			case 2: treasure = TreasureType.START_02;
			case 3: treasure = TreasureType.START_03;
			case 4: treasure = TreasureType.START_04;
			}

			PositionType homePos = b.findTreasure(treasure);

			List<PositionType> allPos = b.getAllReachablePositions(myPos);
			if (allPos.contains(homePos))
				val -= Values.ENEMY_CAN_FINISH;
		} else {
			val -= (getReachableTreasures(myPos, b) * 1. / allTreasures) * Values.TREASURES_AVAILABLE;
		}
		return val;
	}

	private static class Node {
		public PositionType pos;
		public int dist = Integer.MAX_VALUE;
	}

	private static class MyComparator implements Comparator<Node> {
		private PositionType treas;
		public MyComparator(PositionType treas) {
			this.treas = treas;
		}

		@Override
		public int compare(Node a, Node b) {
			if (a.dist < b.dist) return -1;
			if (a.dist > b.dist) return 1;
			int dist1 = Math.abs(a.pos.getCol() - treas.getCol());
			dist1 += Math.abs(a.pos.getRow() - treas.getRow());
			int dist2 = Math.abs(b.pos.getCol() - treas.getCol());
			dist2 += Math.abs(b.pos.getRow() - treas.getRow());
			if (dist1 < dist2) return -1;
			if (dist1 > dist2) return 1;
			return 0;
		}
	}

	// gibt den Pfad durch das Labyrinth zurueck, der vom Spieler bis zum
	// Schatz durch am wenigsten Waende geht
	private int getWallsToTreasure(PositionType from, PositionType to, Board b) {
		for (int i = 0; i < 7; i++) {
			for (int j = 0; j < 7; j++)
				nodes[i][j].dist = Integer.MAX_VALUE;
		}

		Node start = nodes[from.getRow()][from.getCol()];
		start.dist = 0;
		PriorityQueue<Node> horizon = new PriorityQueue<Node>(10, new MyComparator(to));
		Set<PositionType> visited = new HashSet<PositionType>();
		horizon.add(start);
		Node n = null;
		while (!horizon.isEmpty()) {
			n = horizon.poll();
			if (n.pos.equals(to))
				break;
			visited.add(n.pos);
			for (Node neighbor : neighbors.get(n.pos)) {
				if (visited.contains(neighbor.pos)) continue;
				int dist = n.dist + getDist(n, neighbor, b);
				if (dist < neighbor.dist) {
					neighbor.dist = dist;
				}
				horizon.add(neighbor);
			}
		}
		return n.dist;
	}

	private int getDist(Node n1, Node n2, Board b) {
		Openings o1 = b.getCard(n1.pos.getRow(), n1.pos.getCol()).getOpenings();
		Openings o2 = b.getCard(n2.pos.getRow(), n2.pos.getCol()).getOpenings();
		int res = 0;
		if (n1.pos.getCol() == n2.pos.getCol()) {
			if (n1.pos.getRow() < n2.pos.getRow()) {
				//n1 ueber n2
				res += o1.isBottom() ? 0 : 1;
				res += o2.isTop() ? 0 : 1;
			} else {
				//n1 unter n2
				res += o1.isTop() ? 0 : 1;
				res += o2.isBottom() ? 0 : 1;
			}
		} else {
			if (n1.pos.getCol() < n2.pos.getCol()) {
				//n1 links von n2
				res += o1.isRight() ? 0 : 1;
				res += o2.isLeft() ? 0 : 1;
			} else {
				//n1 rechts von n2
				res += o1.isLeft() ? 0 : 1;
				res += o2.isRight() ? 0 : 1;
			}
		}
		return res;
	}

	private boolean validPos(int row, int col) {
		return 0 <= row && row <= 6 && 0 <= col && col <= 6;
	}

	private int getReachableTreasures(PositionType pos, Board b) {
		int res = 0;
		Set<TreasureType> treasures = trKpr.getAvailableTreasures();
		List<PositionType> fields = b.getAllReachablePositions(pos);
		for (PositionType field : fields) {
			CardType c = b.getCard(field.getRow(), field.getCol());
			if (treasures.contains(c.getTreasure())) res++;
		}

		return res;
	}

	private boolean onTreasure(Board board, int player) {
		if (player == id) {
			PositionType trPos = board.findTreasure(treasure);
			if (trPos == null) return false;
			return trPos.equals(board.findPlayer(player));
		}
		else {
			int treasures = 0;
			for (TreasuresToGoType t : treasuresToGo) {
				if (t.getPlayer() == player)
					treasures = t.getTreasures();
			}
			if (treasures == 1) {
				TreasureType treasure = null;
				switch (player) {
				case 1: treasure = TreasureType.START_01;
				case 2: treasure = TreasureType.START_02;
				case 3: treasure = TreasureType.START_03;
				case 4: treasure = TreasureType.START_04;
				}
				return board.findTreasure(treasure).equals(board.findPlayer(player));
			} else
				return false;
		}
	}

}